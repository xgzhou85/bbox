# ================================================================
# standard Bluespec setup, defining environment variables
# BLUESPECDIR, BLUESPEC_HOME and BLUESPEC_LICENSE_FILE,
# and placing $BLUESPEC_HOME/bin in your path is expected to
# invoke 'bsc', the Bluespec compiler.
# ================================================================

#Targeting Config
CONFIG ?= block_config.inc
include $(CONFIG)
#Targeting Design
# Directory containing the box
TOPFILE?=Tb_bitmanip.bsv
TOPMODULE?=mkTb_bitmanip
TOPDIR?=./testbench

BSC_DIR := $(shell which bsc)
BSC_VDIR:=$(subst /bsc,/,${BSC_DIR})../lib/Verilog/

# Verilog simulator

# ================================================================
ifeq ($(XLEN), 32)
	override define_macros += -D RV32=True
         XLEN=32
else
	override define_macros += -D RV64=True
         XLEN=64
endif

ifeq ($(ZBS), enable)
        override define_macros += -D ZBS=True
                BMI_ALL=0
endif

ifeq ($(ZBB), enable)
	override define_macros += -D ZBB=True
		BMI_ALL=0
endif

ifeq ($(ZBA), enable)
	override define_macros += -D ZBA=True
		BMI_ALL=0
endif
ifeq ($(ZBC), enable)
	override define_macros += -D ZBC=True
		BMI_ALL=0
endif
ifeq ($(ZBM), enable)
	override define_macros += -D ZBM=True
		BMI_ALL=0
endif

ifeq ($(ZBT), enable)
	override define_macros += -D ZBT=True
		BMI_ALL=0
endif
ifeq ($(ZBR), enable)
	override define_macros += -D ZBR=True
		BMI_ALL=0
endif
ifeq ($(ZBE), enable)
	override define_macros += -D ZBE=True
		BMI_ALL=0
endif
ifeq ($(ZBP), enable)
	override define_macros += -D ZBP=True
		BMI_ALL=0
endif
ifeq ($(ZBF), enable)
	override define_macros += -D ZBF=True
		BMI_ALL=0
endif
ifeq ($(BMI_ALL), enable)
	override define_macros += -D ZBB=True -D ZBA=True -D ZBS=True -D ZBC=True -D ZBM=True -D ZBT=True -D ZBR=True -D ZBE=True -D ZBP=True -D ZBF=True
endif
ifneq (0,$(VERBOSITY))
	VERILATOR_FLAGS += -DVERBOSE
	VCS_MACROS += +define+VERBOSE=True
endif

ifeq ($(SYNTH), FPGA)
	TOPDIR    = ./src
	TOPFILE   = bitmanip.bsv
	TOPMODULE = mkbitmanip
default: generate_verilog
else
	override define_macros += -D simulate=True
default: generate_verilog link_verilator simulate
endif


FILES:= ./src:./testbench/
VERILOGDIR := ./verilog
BSVOUTDIR := ./bin
BUILDDIR := bsv_build
BSC_COMP_FLAGS = -u -verilog -elab -vdir $(VERILOGDIR) -bdir $(BUILDDIR) -info-dir $(BUILDDIR) \
								 +RTS -K40000M -RTS -aggressive-conditions -no-warn-action-shadowing -check-assert  -keep-fires  -opt-undetermined-vals \
								 -remove-false-rules -remove-empty-rules -remove-starved-rules -remove-dollar \
								 -unspecified-to X -show-schedule -show-module-use


BSC_PATHS = -p ./:./common_bsv:%/Libraries:$(FILES)

# ----------------------------------------------------------------
# Verilog compile/link/sim
# ----------------------------------------------------------------
# ----------------- Setting up flags for verilator ---------------------------------------------- #
VERILATESIM := -CFLAGS -O3
THREADS=1
SIM_MAIN=./testbench/sim_main.cpp
SIM_MAIN_H=./testbench/sim_main.h
VERILATOR_FLAGS += --stats -O3 $(VERILATESIM) -LDFLAGS "-static" --x-assign fast \
					--x-initial fast --noassert $(SIM_MAIN) --bbox-sys -Wno-STMTDLY \
					-Wno-UNOPTFLAT -Wno-WIDTH -Wno-lint -Wno-COMBDLY -Wno-INITIALDLY --trace\
					--autoflush --threads $(THREADS) -DBSV_RESET_FIFO_HEAD -DBSV_RESET_FIFO_ARRAY
# ---------------------------------------------------------------------------------------------- #
.PHONY: help
help:
	@echo "Current settings"
	@echo "    BLUESPEC_HOME         = " $(BSC_DIR)
	@echo "    BLUESPECDIR           = " $(BLUESPECDIR)
	@echo ""
	@echo "    EXAMPLE               = " $(TOPFILE)
	@echo "    ARCHITECTURE          = " $(XLEN)
	@echo ""
	@echo "Targets for 'make':"
	@echo "    help                Print this information"
	@echo ""
	@echo ""
	@echo "    Verilog generation and Verilog sim:"
	@echo "     generate_verilog   Compile for Verilog (Verilog files generated in verilog_dir/)"
	@echo "     link_verilator     Link Verilog simulation executable"
	@echo "     simulate      	   Run the Verilog simulation executable"
	@echo "     all          	   Convenience for make generate_verilog link_verilator simulate"
	@echo ""
	@echo "    clean               clears files generated during build"


check-env:
ifeq (, $(shell which bsc))
	$(error "BSC not found in $(PATH). Exiting ")
endif

.PHONY: all
ifeq ($(SYNTH), FPGA)
all: clean  generate_verilog
else
all: clean  generate_verilog link_verilator simulate
endif

.PHONY: generate_verilog
generate_verilog: check-env
	@echo Compiling for Verilog ...
	@mkdir -p $(BUILDDIR) $(VERILOGDIR)
	bsc $(BSC_COMP_FLAGS) -D VERBOSITY=$(VERBOSITY) $(define_macros)  $(BSC_PATHS) -g $(TOPMODULE)  $(TOPDIR)/$(TOPFILE) || (echo "BSC COMPILE ERROR"; exit 1)
	@echo Compiling for Verilog finished

.PHONY: link_verilator
link_verilator: ## Generate simulation executable using Verilator
	@echo "Linking $(TOPMODULE) using verilator"
	@mkdir -p $(BSVOUTDIR) obj_dir
	@echo "#define TOPMODULE V$(TOPMODULE)" > $(SIM_MAIN_H)
	@echo '#include "V$(TOPMODULE).h"' >> $(SIM_MAIN_H)
	verilator $(VERILATOR_FLAGS) --cc $(VERILOGDIR)/$(TOPMODULE).v -y $(VERILOGDIR) \
		-y $(BSC_VDIR) --exe
	@ln -f -s $(SIM_MAIN) obj_dir/sim_main.cpp
	@ln -f -s $(SIM_MAIN_H) obj_dir/sim_main.h
	@make -j8 -C obj_dir -f V$(TOPMODULE).mk
	@cp obj_dir/V$(TOPMODULE) $(BSVOUTDIR)/out
	@echo Linking for Verilog sim finished

.PHONY: simulate
simulate:
	@echo Simulation...
	$(BSVOUTDIR)/out +fullverbose > log
	@echo Simulation finished

# ----------------------------------------------------------------

.PHONY: clean
clean:
	rm -rf  $(BSVOUTDIR) obj_dir  $(VERILOGDIR) bin *~
	rm -f  *$(TOPMODULE)*  *.vcd

