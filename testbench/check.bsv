function Bit#(64) fn_checktest_64( Reg#(Bit#(32)) i );
//return action
   case(i)
     0 : return(1);
     1 : return(pack(64'hfffffffffffffffe));
     2 : return(pack(64'hffffffffffffffff));
     3 : return(pack(64'hdeadbeefff));
     4 : return(pack(64'hff00000000deadbe));
     5 : return(pack(64'hdeadbeef0));
     6 : return(pack(64'hf00000000deadbee));
     7 : return(pack(64'hdeadbeed));
     8 : return(pack(64'h0));
     9 : return(pack(64'h2A1CCA08));
     10: return(pack(64'h8000000b3d3eff));

     11 : return(pack(64'hcafebbbbdeadbeef));
     12 : return(pack(64'h0));
     13 : return(pack(64'hdeadbeeff));
     14 : return(pack(64'hf00000000deadbee));
     15 : return(pack(64'hf00000000deadbee));
     16 : return(pack(64'h3));
     17 : return(pack(64'h5));
     18 : return(pack(64'h9));
     19 : return(pack(64'hfffffffffffffffc));
     20 : return(pack(64'h5));

     21 : return(pack(64'h8));
     22 : return(pack(64'hfffffffffffffffc));
     23 : return(pack(64'hffff1112));
     24 : return(pack(64'hffff1110));
     25 : return(pack(64'hffff0000ffff1112));
     26 : return(pack(64'hffff0000ffff1110));
     27 : return(pack(64'h2341234f));
     28 : return(pack(64'hfffffffff1234123));
     29 : return(pack(64'h41234123));
     30 : return(pack(64'h23412341));

     31 : return(pack(64'h21432143));
     32 : return(pack(64'hdeadbeed));
     33 : return(pack(64'hdeadbeed));
     34 : return(pack(64'h1));
     35 : return(pack(64'h1e5a9b3d));
     36 : return(pack(64'h21432143));
     37 : return(pack(64'h33773377));
     38 : return(pack(64'h333c333c));
     39 : return(pack(64'h33773377));
     40 : return(pack(64'h21432143));

     41 : return(pack(64'h21382138));
     42 : return(pack(64'h33773377));
     43 : return(pack(64'hffffffffdeadbeed));
     44 : return(pack(64'h0));
     45 : return(pack(64'hffffffffdeadbeff));
     46 : return(pack(64'hffffffffdeadbeeb));
     47 : return(pack(64'hffffffffdeadbeed));
     48 : return(pack(64'hffffffffdeadbeef));
     49 : return(pack(64'hffffffffeadbeeff));
     50 : return(pack(64'hfffffffffdeadbee));

     51 : return(pack(64'hfffffffffdeadbee));
     52 : return(pack(64'hffffffffdeadbeed));
     53 : return(pack(64'h20));
     54 : return(pack(64'h2));
     55 : return(pack(64'ha));
     56 : return(pack(64'h8));
     57 : return(pack(64'h0));
     58 : return(pack(64'h0));
     59 : return(pack(64'h3));
     60 : return(pack(64'h5));

     61 : return(pack(64'h9));
     62 : return(pack(64'h2));
     63 : return(pack(64'h2));
     64 : return(pack(64'h2));
     65 : return(pack(64'h2));
     66 : return(pack(64'h8));
     67 : return(pack(64'h0));
     68 : return(pack(64'h0));
     69 : return(pack(64'h3));
     70 : return(pack(64'h4));

     71 : return(pack(64'h3));
     72 : return(pack(64'h4));
     73 : return(pack(64'h5d681b02));
     74 : return(pack(64'hee530937));
     75 : return(pack(64'he9dbf6c3));
     76 : return(pack(64'h8770fe41));
     77 : return(pack(64'h988c474d));
     78 : return(pack(64'hfecbdd28));
     79 : return(pack(64'h8e647309));
     80 : return(pack(64'hf950bce6));

     81 : return(pack(64'hbbef));
     82 : return(pack(64'h13241324));
     83 : return(pack(64'h13241324));
     84 : return(pack(64'h14521452));
     85 : return(pack(64'h14521452));
     86 : return(pack(64'h13241324));
     87 : return(pack(64'h13241324));
     88 : return(pack(64'h4));
     89 : return(pack(64'h4));
     90 : return(pack(64'h41234));

     91 : return(pack(64'h1234));
     92 : return(pack(64'h2));
     93 : return(pack(64'hffffffff80000001));
     94 : return(pack(64'h6));
     95 : return(pack(64'h2));
     96 : return(pack(64'ha));
     97 : return(pack(64'h34));
     98 : return(pack(64'h1234));
     99 : return(pack(64'h55ff0055aa00));
     100 : return(pack(64'h2612261226122612));

     101 : return(pack(64'h3636363636363636));
     102 : return(pack(64'hffffffff80000000));
     103 : return(pack(64'h9EA2234));
     104 : return(pack(64'hffff1112));
     105 : return(pack(64'h1fffe2222));
     default:return 0;
endcase
//endaction;
endfunction

function Bit#(32) fn_checktest_32( Reg#(Bit#(32)) i );
//return action
   case(i)
     0 : return(1);
     1 : return(pack(32'hfffffffe));
     2 : return(pack(32'hffffffff));
     3 : return(pack(32'hadbeefff));
     4 : return(pack(32'hffdeadbe));
     5 : return(pack(32'headbeefd));
     6 : return(pack(32'hfdeadbee));
     7 : return(pack(32'hdeadbeed));
     8 : return(pack(32'h0));
     9 : return(pack(32'h2A1CCA08));
     10: return(pack(32'hBBD3EFF));



     11 : return(pack(32'hbbbbbeef));
     12 : return(pack(32'hcafedead));
     13 : return(pack(32'headbeeff));
     14 : return(pack(32'hfdeadbee));
     15 : return(pack(32'hfdeadbee));
     16 : return(pack(32'h3));
     17 : return(pack(32'h5));
     18 : return(pack(32'h9));
     19 : return(pack(32'hfffffffc));
     20 : return(pack(32'h5));

     21 : return(pack(32'h8));
     22 : return(pack(32'hfffffffc));
     23 : return(pack(32'h0));
     24 : return(pack(32'h0));
     25 : return(pack(32'h0));
     26 : return(pack(32'h0));
     27 : return(pack(32'h0));
     28 : return(pack(32'h0));
     29 : return(pack(32'h0));
     30 : return(pack(32'h0));

     31 : return(pack(32'h21432143));
     32 : return(pack(32'hdeadbeed));
     33 : return(pack(32'hdeadbeed));
     34 : return(pack(32'h1));
     35 : return(pack(32'h1e5a9b3d));
     36 : return(pack(32'h21432143));
     37 : return(pack(32'h33773377));
     38 : return(pack(32'h333c333c));
     39 : return(pack(32'h0));
     40 : return(pack(32'h0));

     41 : return(pack(32'h0));
     42 : return(pack(32'h0));
     43 : return(pack(32'h0));
     44 : return(pack(32'h0));
     45 : return(pack(32'h0));
     46 : return(pack(32'h0));
     47 : return(pack(32'h0));
     48 : return(pack(32'h0));
     49 : return(pack(32'h0));
     50 : return(pack(32'h0));

     51 : return(pack(32'h0));
     52 : return(pack(32'h0));
     53 : return(pack(32'h0));
     54 : return(pack(32'h2));
     55 : return(pack(32'ha));
     56 : return(pack(32'h8));
     57 : return(pack(32'h0));
     58 : return(pack(32'h0));
     59 : return(pack(32'h0));
     60 : return(pack(32'h0));

     61 : return(pack(32'h0));
     62 : return(pack(32'h2));
     63 : return(pack(32'h2));
     64 : return(pack(32'h2));
     65 : return(pack(32'h2));
     66 : return(pack(32'h0));
     67 : return(pack(32'h0));
     68 : return(pack(32'h0));
     69 : return(pack(32'h3));
     70 : return(pack(32'h4));

     71 : return(pack(32'h0));
     72 : return(pack(32'h0));
     73 : return(pack(32'h5d681b02));
     74 : return(pack(32'hee530937));
     75 : return(pack(32'he9dbf6c3));
     76 : return(pack(32'h0));
     77 : return(pack(32'h988c474d));
     78 : return(pack(32'hfecbdd28));
     79 : return(pack(32'h8e647309));
     80 : return(pack(32'h0));

     81 : return(pack(32'hbbef));
     82 : return(pack(32'h13241324));
     83 : return(pack(32'h13241324));
     84 : return(pack(32'h14521452));
     85 : return(pack(32'h14521452));
     86 : return(pack(32'h0));
     87 : return(pack(32'h0));
     88 : return(pack(32'h12340004));
     89 : return(pack(32'h0));
     90 : return(pack(32'h0));

     91 : return(pack(32'h0));
     92 : return(pack(32'h0));
     93 : return(pack(32'h0));
     94 : return(pack(32'h0));
     95 : return(pack(32'h0));
     96 : return(pack(32'h0));
     97 : return(pack(32'h34));
     98 : return(pack(32'h1234));
     99 : return(pack(32'h0));
     100 : return(pack(32'h0));

     101 : return(pack(32'h0));
     102 : return(pack(32'h0));
     103 : return(pack(32'h9EA2234));
     104 : return(pack(32'h0));
     105 : return(pack(32'h0));
  //   default:`logLevel( bitmanip,1,$format ("\t Sent ILLEGAL INSTR to dut"))
endcase
//endaction;
endfunction
