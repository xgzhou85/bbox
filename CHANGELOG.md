# CHANGELOG
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.17] - 2020-04-08
### Changed
    - FSRI/GREVI Fix

## [0.5.16] - 2020-03-31
### Changed
    - ZBS Optimized

## [0.5.15] - 2020-03-27
### Changed   
  - Op-32 operations Fix

## [0.5.14] - 2020-03-27
### Changed
  - SL(R)O/SL(R)OI Bug Fix

## [0.5.13] - 2020-03-13
### Changed
  - FSRI opcode changed

## [0.5.12] - 2020-03-12
### Changed
  - Fixed CMOV Instruction fuctionality


## [0.5.11] - 2020-03-10
### Changed
  - Fixed FSR/FSL/FSRI Instruction fuctionality
  - upgraded Shifter width
  - Updated README

## [0.5.10] - 2020-02-2
### Changed
  - fixed MIN/MAX Instruction functionality

## [0.5.9] - 2020-02-28
### Changed
  - added comments for readability

## [0.5.8] - 2020-02-28
### Changed
  - fixed W-instructions bugs
  - fixed fsr 
  - reduced function calls to the maximum to optimise logic

 ## [0.5.7] - 2020-02-28
### Changed
- Modified testbench to include SBSET and SBINV

## [0.5.6] - 2020-02-26
### Changed
- Bug Fixes in Zbs

## [0.5.7] - 2020-02-28
### Changed
- Modified testbench to include SBSET and SBINV

## [0.5.6] - 2020-02-26
### Changed
- Bug Fixes in Zbs

## [0.5.5] - 2020-02-26
### Changed
 - SHFL/UNSHFL Bugs fixed
 - Mask data in ZBP statically elaborated
 - ZBP *W instructions were complied with riscv-spec


## [0.5.5] - 2020-02-26
### Changed
 - SHFL/UNSHFL Bugs fixed
 - Mask data in ZBP statically elaborated
 - ZBP *W instructions were complied with riscv-spec

## [0.5.4] - 2020-02-25
### Changed
- Bug Fixes in scop_busy 

## [0.5.3] - 2020-02-20
### Changed
- ZBB optimized 

## [0.5.2] - 2020-02-18
### Changed
- moved compilation to use the new open-bsc compiler

## [0.5.1] - 2020-02-04
### Fixed
- restructured bitmanip computation unit to avoid duplicate function calls
- removed all tagged Valid ; changed to tuple


## [0.5.0] - 2020-02-04
### Fixed
- Changed the bitmanip.bsv module to be a single method interface since all ops are single cycle. 
- Changed the test-bench to accommodate the above changes in the bitmanip.bsv module
- Duplicate path warning for testbench folder has been fixed.

## [0.4.2] - 2020-01-29

### Fixed 

- Buffered Output in bitmanip.bsv and Tb_bitmanip.bsv

## [0.4.1] - 2020-01-22

### Fixed 
- Fixed Makefile for verilog release

## [0.4.0] - 2020-01-22

### Fixed 
- updated bitmanip.bsv for output format and associated testbench Tb_bitmanip.bsv
- changed file name op_32 -> compute_bmi
- instruction naming and indentation in *.defines
- Makefile changes to fix Verbosity/Simulate

## [0.3.2] - 2020-01-20

### Fixed 
- updated CI to fix logger and to run manager.sh

## [0.3.1] - 2020-01-08
### Fixed 
- fixed typo in ci


## [0.3.0] - 2020-01-07
### Fixed 
- updated CI to release verilog files 
- updated Makefile with release target


## [0.2.3] - 2020-01-02
### Fixed 
- bitmanip.bsv, Makefile, Readme, ChangeLog

## [0.2.2] - 2019-12-24
### Fixed 
- .gitlab-ci.yml

## [0.2.1] - 2019-12-24
### Added 
- sim_main, Makefile, Readme, CHANGELOG

## [0.2.0] - 2019-12-23
### Added 
- doc, ci, CHANGELOG

## [0.1.0] - 2019-12-23
### Added 
- Initial bbox design commt

# CHANGELOG

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.2] - 2020-02-18
### Changed
- moved compilation to use the new open-bsc compiler

## [0.5.1] - 2020-02-04
### Fixed
- restructured bitmanip computation unit to avoid duplicate function calls
- removed all tagged Valid ; changed to tuple


## [0.5.0] - 2020-02-04
### Fixed
- Changed the bitmanip.bsv module to be a single method interface since all ops are single cycle. 
- Changed the test-bench to accommodate the above changes in the bitmanip.bsv module
- Duplicate path warning for testbench folder has been fixed.

## [0.4.2] - 2020-01-29

### Fixed 

- Buffered Output in bitmanip.bsv and Tb_bitmanip.bsv

## [0.4.1] - 2020-01-22

### Fixed 
- Fixed Makefile for verilog release

## [0.4.0] - 2020-01-22

### Fixed 
- updated bitmanip.bsv for output format and associated testbench Tb_bitmanip.bsv
- changed file name op_32 -> compute_bmi
- instruction naming and indentation in *.defines
- Makefile changes to fix Verbosity/Simulate

## [0.3.2] - 2020-01-20

### Fixed 
- updated CI to fix logger and to run manager.sh

## [0.3.1] - 2020-01-08
### Fixed 
- fixed typo in ci


## [0.3.0] - 2020-01-07
### Fixed 
- updated CI to release verilog files 
- updated Makefile with release target


## [0.2.3] - 2020-01-02
### Fixed 
- bitmanip.bsv, Makefile, Readme, ChangeLog

## [0.2.2] - 2019-12-24
### Fixed 
- .gitlab-ci.yml

## [0.2.1] - 2019-12-24
### Added 
- sim_main, Makefile, Readme, CHANGELOG

## [0.2.0] - 2019-12-23
### Added 
- doc, ci, CHANGELOG

## [0.1.0] - 2019-12-23
### Added 
- Initial bbox design commt


