  /*doc:func: The cmix rd, rs2, rs1, rs3 instruction selects bits from rs1 and rs3 based on the bits in the
control word rs2.*/
  function Bit#(XLEN) fn_cmix(Bit#(XLEN) src1,Bit#(XLEN) src2, Bit#(XLEN) src3) provisos(Bitwise#(Bit#(XLEN))) ; 
    return ((src1 & src2) | (src3 & (~src2)) );
  endfunction
  /*doc:func:The cmov rd, rs2, rs1, rs3 instruction selects rs1 if the control word rs2 is non-zero, and rs3
if the control word is zero.*/
  function Bit#(XLEN) fn_cmov(Bit#(XLEN) src1,Bit#(XLEN) src2, Bit#(XLEN) src3) provisos(Bitwise#(Bit#(XLEN))) ; 
    return ( (src2>0) ? src1 : src3 );
  endfunction
  /*doc:func:The fsl rd, rs1, rs3, rs2 instruction creates a 2 · XLEN word by concatenating rs1 and rs3
(with rs1 in the MSB half), rotate-left-shifts that word by the amount indicated in the log 2 (XLEN)+
1 LSB bits in rs2, and then writes the MSB half of the result to rd.*/
  function Bit#(XLEN) fn_fsl(Bit#(XLEN) src1,Bit#(XLEN) src2, Bit#(XLEN) src3) provisos(Bitwise#(Bit#(XLEN))) ; 

    let fsl ={src1,src3};	
    let shamt = src2 & (2*fromInteger(valueOf(XLEN))-1);
    fsl=rotateBitsBy(fsl,truncate(fromSizedInteger(shamt)));
    //fsl = (fsl& 64'hffffffff00000000) >>32;	
   `ifdef RV32 return (zeroExtend(fsl[63:32])); `endif
   `ifdef RV64 return(zeroExtend(fsl[127:64])); `endif
  endfunction
  /*doc:func:The fsr rd, rs1, rs3, rs2 instruction creates a 2 · XLEN word by concatenating rs1 and
rs3 (with rs1 in the LSB half), rotate-right-shifts that word by the amount indicated in the
log 2 (XLEN) + 1 LSB bits in rs2, and then writes the LSB half of the result to rd.*/
  function Bit#(XLEN) fn_fsr(Bit#(XLEN) src1,Bit#(XLEN) src2, Bit#(XLEN) src3) provisos(Bitwise#(Bit#(XLEN))) ; 
    let fsl ={src3,src1};	
    let shamt = src2 & (2*fromInteger(valueOf(XLEN))-1);
    let maxLen = fromInteger(valueof(XLEN));
    fsl=rotateBitsBy(fsl,truncate(fromSizedInteger((2*maxLen)-shamt)));
    //fsl = (fsl& 64'h00000000ffffffff);	
    `ifdef RV32 return (zeroExtend(fsl[31:0])); `endif
    `ifdef RV64 return(zeroExtend(fsl[63:0])); `endif
  endfunction
  
    /*doc:func:The fsr rd, rs1, rs3, rs2 instruction creates a 2 · XLEN word by concatenating rs1 and
rs3 (with rs1 in the LSB half), rotate-right-shifts that word by the amount indicated in the
log 2 (XLEN) + 1 LSB bits in rs2, and then writes the LSB half of the result to rd.*/
    function Bit#(XLEN) fn_fsri(Bit#(XLEN) src1, Bit#(6) imm,Bit#(XLEN) src3) provisos(Bitwise#(Bit#(XLEN))) ; 
    let fsl ={src3,src1};	
    Bit#(XLEN) shamt = zeroExtend(imm) ;
    shamt = shamt & (2*fromInteger(valueOf(XLEN))-1);
    let maxLen = fromInteger(valueof(XLEN));
    fsl=rotateBitsBy(fsl,truncate(fromSizedInteger((2*maxLen)-shamt)));
    //fsl = (fsl& 64'h00000000ffffffff);	
    `ifdef RV32 return (zeroExtend(fsl[31:0])); `endif
    `ifdef RV64 return(zeroExtend(fsl[63:0])); `endif
  endfunction

  `ifdef RV64
   /*doc:func:The fsl rd, rs1, rs3, rs2 instruction creates a 2 · XLEN word by concatenating rs1 and rs3
(with rs1 in the MSB half), rotate-left-shifts that word by the amount indicated in the log 2 (XLEN)+
1 LSB bits in rs2, and then writes the MSB half of the result to rd.*/
   function Bit#(XLEN) fn_fslw(Bit#(XLEN_BY_2) src1,Bit#(XLEN_BY_2) src2, Bit#(XLEN_BY_2) src3) provisos(Bitwise#(Bit#(XLEN))) ; 

    let fsl ={src1,src3};	
    let shamt = src2 & (2*fromInteger(valueOf(XLEN_BY_2))-1);
    fsl=rotateBitsBy(fsl,truncate(fromSizedInteger(shamt)));
    //fsl = (fsl& 64'hffffffff00000000) >>32;	
    return signExtend(fsl[63:32] );
  endfunction
  /*doc:func:The fsr rd, rs1, rs3, rs2 instruction creates a 2 · XLEN word by concatenating rs1 and
rs3 (with rs1 in the LSB half), rotate-right-shifts that word by the amount indicated in the
log 2 (XLEN) + 1 LSB bits in rs2, and then writes the LSB half of the result to rd.*/
  function Bit#(XLEN) fn_fsrw(Bit#(XLEN_BY_2) src1,Bit#(XLEN_BY_2) src2, Bit#(XLEN_BY_2) src3) provisos(Bitwise#(Bit#(XLEN))) ; 
    let fsl ={src3,src1};	
    let shamt = src2 & (2*fromInteger(valueOf(XLEN_BY_2))-1);
    let maxLen = fromInteger(valueof(XLEN_BY_2));
    fsl=rotateBitsBy(fsl,truncate(fromSizedInteger((2*maxLen)-shamt)));
    //fsl = (fsl& 64'h00000000ffffffff);	
    return signExtend(fsl[31:0] );
  endfunction
  /*doc:func:The fsr rd, rs1, rs3, rs2 instruction creates a 2 · XLEN word by concatenating rs1 and
rs3 (with rs1 in the LSB half), rotate-right-shifts that word by the amount indicated in the
log 2 (XLEN) + 1 LSB bits in rs2, and then writes the LSB half of the result to rd.*/
  function Bit#(XLEN) fn_fsriw(Bit#(XLEN_BY_2) src1, Bit#(5) imm,Bit#(XLEN_BY_2) src3) provisos(Bitwise#(Bit#(XLEN))) ; 
    let fsl ={src3,src1};	
    Bit#(XLEN_BY_2) shamt = zeroExtend(imm) ;
    shamt = shamt & (2*fromInteger(valueOf(XLEN_BY_2))-1);
    let maxLen = fromInteger(valueof(XLEN_BY_2));
    fsl=rotateBitsBy(fsl,truncate(fromSizedInteger((2*maxLen)-shamt)));
   // fsl = (fsl& 64'h00000000ffffffff);	
    return signExtend(fsl[31:0] );
  endfunction
  `endif 
