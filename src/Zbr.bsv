/*doc:func:Unary Cyclic Redundancy Check (CRC) instructions that interpret the bits of rs1 as a
CRC32/CRC32C state and perform a polynomial reduction of that state shifted left by 8, 16,32, or 64 bits.*/
function Bit#(XLEN) fn_crc32(Bit#(XLEN) src1, int nbits) provisos(Bitwise#(Bit#(XLEN))) ;
    for (int i = 0; i < nbits; i=i+1)begin
      src1 = (src1 >> 1) ^ ('hEDB88320 & signExtend(src1[0]));
    end
    return src1;
  endfunction
/*doc:func:Unary Cyclic Redundancy Check (CRC) instructions that interpret the bits of rs1 as a
CRC32/CRC32C state and perform a polynomial reduction of that state shifted left by 8, 16,32, or 64 bits.*/
function Bit#(XLEN) fn_crc32c(Bit#(XLEN) src1, int nbits) provisos(Bitwise#(Bit#(XLEN))) ;
    for (int i = 0; i < nbits; i=i+1)begin
      src1 = (src1 >> 1) ^ ('h82F63B78 & signExtend(src1[0]));
    end
    return src1;
  endfunction
