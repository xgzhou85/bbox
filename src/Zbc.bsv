 /*doc:func: Calculate the carry-less product [22] of the two arguments. clmul produces the lower half of the
carry-less product*/
function Bit#(XLEN) fn_clmul(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN)), Ord#(Bit#(XLEN))) ; 
    Bit#(XLEN) x= 0;
    for(Integer i=0; i<valueof(XLEN); i=i+1)begin
      if(((src2 >> i) & 1) == 1 )
          begin
           x = x ^ (src1 << i);
           end	
          end
     let regd = pack(x);
    return signExtend(regd);
  endfunction
  /*doc:func:  clmulr produces bits 2·XLEN−2:XLEN-1 of the 2·XLEN carry-less product*/
  function Bit#(XLEN) fn_clmulr(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(XLEN) x= 0;
    for(Integer i=0; i<valueof(XLEN); i=i+1)begin
     if(((src2 >> i) & 1) == 1 )
        begin
        x = x ^ (src1 >> valueof(XLEN)-i-1);	
        end
    end
    let regd = pack(x);
    return signExtend(regd);
   endfunction
   /*doc:func:clmulh produces the upper half of the 2·XLEN carry-less product*/
  function Bit#(XLEN) fn_clmulh(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(XLEN) x= 0;
    for(Integer i=0; i<valueof(XLEN); i=i+1)begin
     if(((src2 >> i) & 1) == 1 )
        begin
        x = x ^ (src1 >> valueof(XLEN)-i);	
        end
    end
    let regd = pack(x);
    return signExtend(regd);
  endfunction
  
  `ifdef RV64
  /*doc:func: Calculate the carry-less product [22] of the two arguments. clmul produces the lower half of the
carry-less product; add a *W variant of clmulh. This is because we expect some code to use 32-bit clmul intrisics, even on 64-bit architectures.*/
  function Bit#(XLEN) fn_clmulw(Bit#(XLEN_BY_2) src1,Bit#(XLEN_BY_2) src2) provisos(Bitwise#(Bit#(XLEN)), Ord#(Bit#(XLEN))) ; 
    Bit#(XLEN_BY_2) x= 0;
    for(Integer i=0; i<valueof(XLEN_BY_2); i=i+1)begin
      if(((src2 >> i) & 1) == 1 )
          begin
           x = x ^ (src1 << i);
           end	
          end
    return signExtend(x) ;
  endfunction
   /*doc:func:  clmulr produces bits 2·XLEN−2:XLEN-1 of the 2·XLEN carry-less product;add a *W variant of clmulh. This is because we expect some code to use 32-bit clmul intrisics, even on 64-bit architectures.*/
  function Bit#(XLEN) fn_clmulrw(Bit#(XLEN_BY_2) src1,Bit#(XLEN_BY_2) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(XLEN_BY_2) x= 0;
    for(Integer i=0; i<valueof(XLEN_BY_2); i=i+1)begin
     if(((src2 >> i) & 1) == 1 )
        begin
        x = x ^ (src1 >> valueof(XLEN_BY_2)-i-1);	
        end
    end
    return signExtend(x) ;
   endfunction
  /*doc:func:clmulh produces the upper half of the 2·XLEN carry-less product; add a *W variant of clmulh. This is because we expect some code to use 32-bit clmul intrisics, even on 64-bit architectures.*/
  function Bit#(XLEN) fn_clmulhw(Bit#(XLEN_BY_2) src1,Bit#(XLEN_BY_2) src2) provisos(Bitwise#(Bit#(XLEN))) ; 
    Bit#(XLEN_BY_2) x= 0;
    for(Integer i=0; i<valueof(XLEN_BY_2); i=i+1)begin
     if(((src2 >> i) & 1) == 1 )
        begin
        x = x ^ (src1 >> valueof(XLEN_BY_2)-i);	
        end
    end
   return signExtend(x) ;
  endfunction
   `endif
