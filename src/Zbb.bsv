typedef enum {OTHER,SLLIUW,FSL,FSR}Instr deriving(Eq,Bits);
Integer maxIndex = (valueof(XLEN)*2)-1;
Integer regWidth = (valueof(XLEN));
Integer maxRegIndex = regWidth-1;

function Bit#(TMul#(XLEN,2)) shifter(Bit#(TMul#(XLEN,2)) data,Bit#(TAdd#(TLog#(XLEN),1)) shamt) provisos(Bitwise#(Bit#(XLEN))) ;
`ifdef RV64
    data = (unpack(shamt[6])) ? {data[63:0],data[maxIndex:64]} : data ;
`endif
    data = (unpack(shamt[5])) ? {data[31:0],data[maxIndex:32]} : data ;
    data = (unpack(shamt[4])) ? {data[15:0],data[maxIndex:16]} : data ;
    data = (unpack(shamt[3])) ? { data[7:0],data[ maxIndex:8]} : data ;
    data = (unpack(shamt[2])) ? { data[3:0],data[ maxIndex:4]} : data ;
    data = (unpack(shamt[1])) ? { data[1:0],data[ maxIndex:2]} : data ;
    return (unpack(shamt[0])) ? {   data[0],data[ maxIndex:1]} : data ;
endfunction

function Bit#(XLEN) fn_shift_and_rotate( Bit#(XLEN) src1,Bit#(TAdd#(TLog#(XLEN),1)) src2,Bit#(XLEN) src3, Bit#(1) w_inst,Bit#(1) imm_inst_n,Bit#(1) shft_dir_right,Bit#(7) funct7) provisos(Bitwise#(Bit#(XLEN))) ;

// (shft/rot, shft_dir_right, imm_inst_n, w_inst)

// 30 14  5  3   Function
// ------------   --------
// 0   0  1  0      slo
// 1   0  1  0      rol
// 0   1  1  0      sro
// 1   1  1  0      ror

// 0   0  0  0      sloi
// 0   1  0  0      sroi
// 1   1  0  0      rori

// 0   0  1  1      slow
// 0   1  1  1      srow
// 1   1  1  1      rorw
// 1   0  1  1      rolw

// 0   1  0  1      sroiw
// 1   1  0  1      roriw

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
// 30 | 29 | 28 | 27 | 26 | 25 | 24 | 5  | 14 |   Function  |
//----|----|----|----|----|----|----|----|----|-------------|
//  0 |  0 |  0 |  0 |  1 |  ? | ?  |  0 |  0 |   slliu.w   |
//  ? |  ? |  ? |  ? |  1 |  0 | ?  |  1 |  0 |     fsl     |
//  ? |  ? |  ? |  ? |  1 |  0 | ?  |  1 |  1 |     fsr     |
//  ? |  ? |  ? |  ? |  1 |  ? | ?  |  0 |  1 |    fsri     |
//~~~~|~~~~|~~~~|~~~~|~~~~|~~~~|~~~~|~~~~|~~~~|~~~~~~~~~~~~~|

    Instr instr=OTHER;
    Bit#(1) flag_slliuw=0;
    if(funct7[6:2]==5'b00001 && imm_inst_n==0) begin
        instr=SLLIUW;
        flag_slliuw=1;
        src1 = ~(zeroExtend(src1[31:0]));   //complement of input as shifting is happening in ones
    end

    if(funct7[1:0]==2'b10 && imm_inst_n==1 && shft_dir_right==0)
        instr=FSL;

    if((funct7[1:0]==2'b10 && unpack(imm_inst_n) && unpack(shft_dir_right)) || (unpack(funct7[1]) && imm_inst_n==0 && unpack(shft_dir_right)))
        instr=FSR;

//    if(imm_inst_n==0)
//        src2 = zeroExtend(imm);

//_____________________________________________
// shamt             |     function           |
//-------------------|------------------------|
//  XLEN-1           |      SHIFT             |
//  XLEN-1           |      ROTATE            |
//  XLEN-1           |      SLLIU.W           |
//  (XLEN/2)-1       |      SHIFT*W,ROTATE*W  |
//  (2*XLEN)-1       |      FSL,FSR           |
//~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~~~~~~~~~|

    Bit#(TAdd#(TLog#(XLEN),1)) shamt = truncate(src2) ;

    if(w_inst==1 && (instr==OTHER)) begin // slow,sloiw,sroiw,srow; rorw,roriw,rolw instructions
        shamt = zeroExtend(shamt[4:0]);
    end

    if ((instr == OTHER && !unpack(w_inst)) || instr==SLLIUW) begin
        shamt = {1'b0,truncate(shamt)};
    end


//  FOR SHIFT OPERATIONS, RIGHT
//      (2*XLEN) = {111....., SRC1...}
//  FOR SHIFT OPERATIONS, LEFT
//      (2*XLEN) = { SRC1...,111.....}
//  FOR ROTATE OPERATIONS,
//      (2*XLEN) = { SRC1...,SRC1.....}
//  FOR *W INSTRUCTIONS,
//      (2*XLEN) = {111.....,SRC1...,111.....,SRC1...}

    /*doc:var: replicate 'src1' for rotate else 1's */
    Bit#(XLEN) mask = (unpack(funct7[5]) ? src1 : -1);

`ifdef RV64
    if(w_inst==1 && instr==OTHER) begin
        mask=(unpack(funct7[5]) ? {src1[31:0],src1[31:0]} : {-1,src1[31:0]});
        src1=(unpack(funct7[5]) ? {src1[31:0],src1[31:0]} : {-1,src1[31:0]});
    end
`endif

    if(instr==FSL || instr==FSR) begin
        mask = src3;
    end

    Bit#(TAdd#(XLEN,XLEN)) inpdata = (unpack(shft_dir_right) ? {mask,src1} : {src1,mask});

    /*doc:var: for left shift/rotate - negate the actual amount of shift */
    shamt = ( unpack(shft_dir_right) ? shamt : negate(shamt) ) ;

    Bit#(TAdd#(XLEN,XLEN))reslt = shifter(inpdata,shamt);

//________________________________________________________________________________
// result                                                  |   FUNCTION          |
//---------------------------------------------------------|---------------------|
// [(2*XLEN)-1:(XLEN)]                                     |   FSL               |
// [(XLEN-1):0]                                            |   FSR,SHIFT ,ROTATE |
// SIGNEXTEND[(XLEN/2):0]                                  |   * W INSTRUCTIONS  |
// COMPLEMENT OF RD(as shifting is happening in ones)      |   slliu.w           |
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|~~~~~~~~~~~~~~~~~~~~~|

    case({w_inst,flag_slliuw}) matches
        2'b00:   return ((unpack(shft_dir_right)) ? reslt[maxRegIndex:0] : reslt[maxIndex:regWidth] );
`ifdef RV64
        2'b10:   return signExtend(reslt[31:0]);
        2'b11:   return zeroExtend(~reslt[31:0]);
`endif
    endcase
endfunction

  /*doc:func: function that combines the AND and Not operation by complimenting the second operand */
  function Bit#(XLEN) fn_andn(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    return src1 & ~src2;
  endfunction

  /*doc:func: function that combines the OR and Not operation by complimenting the second operand  */
  function Bit#(XLEN)  fn_orn(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    return src1 | ~src2;
  endfunction

  /*doc:func: function that combines the XOR and Not operation by complimenting the second operand  */
  function Bit#(XLEN) fn_xorn(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
   return src1 ^ ~src2;
  endfunction

  /*doc:func:  For sign-extending a byte or half-word we define two unary instructions*/
  function Bit#(XLEN) fn_sext_b(Bit#(XLEN) src1) provisos(Bitwise#(Bit#(XLEN))) ;
     return signExtend(src1[7:0]);
  endfunction

  /*doc:func:  For sign-extending a byte or half-word we define two unary instructions*/
  function Bit#(XLEN) fn_sext_h(Bit#(XLEN) src1) provisos(Bitwise#(Bit#(XLEN))) ;
     return signExtend(src1[15:0]);
  endfunction

   /*doc:func: function that packs the XLEN/2-bit lower halves of rs1 and rs2 into rd, with rs1 in the lower half and rs2 in the upper half */
 function Bit#(XLEN) fn_pack_lower(Bit#(XLEN) src1,Bit#(XLEN) src2,Bit#(1) op3) provisos(Bitwise#(Bit#(XLEN))) ;
    Bit#(XLEN) rs=0;
`ifdef RV64
     Bit#(XLEN_BY_2) rsw=0;
`endif
`ifdef RV32
     rs= {src2[15:0],src1[15:0]};
`endif
`ifdef RV64
     if(op3==1)
        rsw= {src2[15:0],src1[15:0]};
     else
        rs = {src2[31:0], src1[31:0]};
`endif
  case(op3) matches
    0: return rs;
`ifdef RV64
    1:return signExtend(rsw);
`endif
   endcase
  endfunction

  /*doc:func: function that packs the upper halves of rs1 and rs2 into rd */
  function Bit#(XLEN) fn_pack_upper(Bit#(XLEN) src1,Bit#(XLEN) src2,Bit#(1) op3) provisos(Bitwise#(Bit#(XLEN))) ;
   Bit#(XLEN) rs=0;
`ifdef RV64
      Bit#(XLEN_BY_2) rsw=0;
`endif
`ifdef RV32
      rs= {src2[31:16],src1[31:16]};
`endif
`ifdef RV64
    if(op3==1)
      rsw= {src2[31:16],src1[31:16]};
    else
      rs= {src2[63:32],src1[63:32]};
`endif
    case(op3) matches
      0: return rs;
`ifdef RV64
      1:return signExtend(rsw);
`endif
    endcase
   endfunction

  /*doc:func: function that packs the LSB bytes of rs1 and rs2 into the 16 LSB bits of rd, zero extending the rest of rd */
  function Bit#(XLEN) fn_packh(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    let rs= {src2[7:0],src1[7:0]};
    return zeroExtend(rs);
  endfunction

  /*doc:func: function that finds the minimum between the two signed operands; done without branching and saves time on calculating the absolute values of signed integers*/
  function Bit#(XLEN) fn_min(Bit#(XLEN) src1,Bit#(XLEN) src2,Bit#(2) funct3) provisos(Ord#(Bit#(XLEN))) ;
    Bit#(XLEN) rs1 = {src1[fromInteger(valueOf(XLEN))-1]^(~funct3[1]),src1[fromInteger(valueOf(XLEN))-2:0]};
    Bit#(XLEN) rs2 = {src2[fromInteger(valueOf(XLEN))-1]^(~funct3[1]),src2[fromInteger(valueOf(XLEN))-2:0]};
    return (unpack(pack(rs1 < rs2) ^ funct3[0])) ? src1 : src2 ;
  endfunction

`ifdef RV64
  /*doc:func:identical to addw except that bits XLEN-1:32 of the result are cleared after the addition. I.e. these instructions zero-extend instead of sign-extend the 32-bit result. */
  function Bit#(XLEN) fn_addwu(Bit#(XLEN) src1,Bit#(XLEN) src2,Bit#(1) f5,Bit#(1) f0,Bit#(1) f32) provisos(Arith#(Bit#(XLEN))) ;
    if(f0==0)
      src2=zeroExtend(src2[31:0]);
    let add = src1 + src2;
    let sub = src1 - src2;
    case ({f5,f0,f32}) matches
      3'b000:return add;
      3'b010:return zeroExtend(add[31:0]);
      3'b100:return sub;
      3'b110:return zeroExtend(sub[31:0]);
      3'b??1:return zeroExtend(add[31:0]);
    endcase
  endfunction
`endif

/*doc:func: counts the number of 0 bits at the MSB end of the argument. That is, the number of 0 bits before the first 1 bit counting      from the most significant bit. If the input is 0, the output is XLEN. If the input is -1, the output is 0. */
function Bit#(XLEN) fn_clz(Bit#(XLEN) src1,Bit#(1) op3) provisos(Bitwise#(Bit#(XLEN))) ;
  Bit#(XLEN) result =0;
  if(op3==1) result= zeroExtend(pack(countZerosMSB(src1[31:0])));
  else  result= zeroExtend(pack(countZerosMSB(src1)));
  case(op3) matches
    0: return result;
`ifdef RV64
    1:return signExtend(result[31:0]);
`endif
  endcase
endfunction

/*doc:func:counts the number of 0 bits at the LSB end of the argument. If the input is 0,the output is XLEN. If the input is -1, the output is 0.*/
function Bit#(XLEN) fn_ctz(Bit#(XLEN) src1,Bit#(1) op3) provisos(Arith#(Bit#(XLEN))) ;
  Bit#(XLEN) result =0;
  if(op3==1)
    result= zeroExtend(pack(countZerosLSB(src1[31:0])));
  else
    result= zeroExtend(pack(countZerosLSB(src1)));
  case(op3) matches
    0: return result;
`ifdef RV64
    1:return signExtend(result[31:0]);
`endif
  endcase
endfunction

  /*doc:func:This instruction counts the number of 1 bits in a register.*/
function Bit#(XLEN) fn_pcnt(Bit#(XLEN) src1,Bit#(1) op3) provisos(Arith#(Bit#(XLEN))) ;
  Bit#(XLEN) result =0;
  if(op3==1)
    result= zeroExtend(pack(countOnes(src1[31:0])));
  else
    result= zeroExtend(pack(countOnes(src1)));
  case(op3) matches
    0: return result;
`ifdef RV64
    1: return signExtend(result[31:0]);
`endif
  endcase
endfunction

/*doc:func: function that returns  don't care for illegal opcodes */
function Bit#(XLEN) fn_defaultcase() ;
   return ?;
endfunction



