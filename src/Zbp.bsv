Integer w_mask_1_left_shift = 'h55555555 ; Integer w_mask_1_right_shift = 'hAAAAAAAA ;
Integer w_mask_2_left_shift = 'h33333333 ; Integer w_mask_2_right_shift = 'hCCCCCCCC ;
Integer w_mask_4_left_shift = 'h0F0F0F0F ; Integer w_mask_4_right_shift = 'hF0F0F0F0 ;
Integer w_mask_8_left_shift = 'h00FF00FF ; Integer w_mask_8_right_shift = 'hFF00FF00 ;
Integer w_mask_16_left_shift= 'h0000FFFF ; Integer w_mask_16_right_shift = 'hFFFF0000 ;

Integer w_mask_1_left_shfl = 'h44444444 ; Integer w_mask_1_right_shfl = 'h22222222 ;
Integer w_mask_2_left_shfl = 'h30303030 ; Integer w_mask_2_right_shfl = 'h0C0C0C0C ;
Integer w_mask_4_left_shfl = 'h0F000F00 ; Integer w_mask_4_right_shfl = 'h00F000F0 ;
Integer w_mask_8_left_shfl = 'h00FF0000 ; Integer w_mask_8_right_shfl = 'h0000FF00 ;

Integer mask_1_left_shift   = `ifdef RV64 'h5555555555555555 `else w_mask_1_left_shift `endif ;
Integer mask_2_left_shift   = `ifdef RV64 'h3333333333333333 `else w_mask_2_left_shift `endif ;
Integer mask_4_left_shift   = `ifdef RV64 'h0F0F0F0F0F0F0F0F `else w_mask_4_left_shift `endif ;
Integer mask_8_left_shift   = `ifdef RV64 'h00FF00FF00FF00FF `else w_mask_8_left_shift `endif ;
Integer mask_16_left_shift  = `ifdef RV64 'h0000FFFF0000FFFF `else w_mask_16_left_shift `endif ;

Integer mask_1_left_shfl    = `ifdef RV64 'h4444444444444444 `else w_mask_1_left_shfl `endif ;
Integer mask_2_left_shfl    = `ifdef RV64 'h3030303030303030 `else w_mask_2_left_shfl `endif ;
Integer mask_4_left_shfl    = `ifdef RV64 'h0F000F000F000F00 `else w_mask_4_left_shfl `endif ;
Integer mask_8_left_shfl    = `ifdef RV64 'h00FF000000FF0000 `else w_mask_8_left_shfl `endif ;

Integer mask_1_right_shift  = `ifdef RV64 'hAAAAAAAAAAAAAAAA `else w_mask_1_right_shift `endif ;
Integer mask_2_right_shift  = `ifdef RV64 'hCCCCCCCCCCCCCCCC `else w_mask_2_right_shift `endif ;
Integer mask_4_right_shift  = `ifdef RV64 'hF0F0F0F0F0F0F0F0 `else w_mask_4_right_shift `endif ;
Integer mask_8_right_shift  = `ifdef RV64 'hFF00FF00FF00FF00 `else w_mask_8_right_shift `endif ;
Integer mask_16_right_shift = `ifdef RV64 'hFFFF0000FFFF0000 `else w_mask_16_right_shift `endif ;

Integer mask_1_right_shfl   = `ifdef RV64 'h2222222222222222 `else w_mask_1_right_shfl `endif ;
Integer mask_2_right_shfl   = `ifdef RV64 'h0C0C0C0C0C0C0C0C `else w_mask_2_right_shfl `endif ;
Integer mask_4_right_shfl   = `ifdef RV64 'h00F000F000F000F0 `else w_mask_4_right_shfl `endif ;
Integer mask_8_right_shfl   = `ifdef RV64 'h0000FF000000FF00 `else w_mask_8_right_shfl `endif ;

`ifdef RV64
Integer mask_32_left_shift  = 'h00000000FFFFFFFF ;
Integer mask_32_right_shift = 'hFFFFFFFF00000000 ;

Integer mask_16_left_shfl   = 'h0000FFFF00000000 ;
Integer mask_16_right_shfl  = 'h00000000FFFF0000 ;
`endif

  /*doc:func:The GORC operation is similar to GREV, except that instead of swapping pairs of bits, GORC
ORs them together, and writes the new value in both positions.*/
function Bit#(XLEN) fn_gorc(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    let shamt = src2 & (fromInteger(valueOf(XLEN))-1);
    if ((shamt & 1) > 0)   src1 = src1 | (((src1 & fromInteger(mask_1_left_shift)) << 1)  | ((src1 & fromInteger(mask_1_right_shift)) >> 1));
    if ((shamt & 2) > 0)   src1 = src1 | (((src1 & fromInteger(mask_2_left_shift)) << 2)  | ((src1 & fromInteger(mask_2_right_shift)) >> 2));
    if ((shamt & 4) > 0)   src1 = src1 | (((src1 & fromInteger(mask_4_left_shift)) << 4)  | ((src1 & fromInteger(mask_4_right_shift)) >> 4));
    if ((shamt & 8) > 0)   src1 = src1 | (((src1 & fromInteger(mask_8_left_shift)) << 8)  | ((src1 & fromInteger(mask_8_right_shift)) >> 8));
    if ((shamt & 16) > 0)  src1 = src1 | (((src1 & fromInteger(mask_16_left_shift)) << 16) | ((src1 & fromInteger(mask_16_right_shift)) >> 16));
`ifdef RV64
    if ((shamt & 32) > 0) src1 = src1 | (((src1 & fromInteger(mask_32_left_shift)) << 32) | ((src1 & fromInteger(mask_32_right_shift)) >> 32));
`endif
    return src1;
endfunction

  /*doc:func:The GORC operation is similar to GREV, except that instead of swapping pairs of bits, GORC
ORs them together, and writes the new value in both positions.*/
function Bit#(XLEN) fn_gorci(Bit#(XLEN) src1,Imm imm) provisos(Bitwise#(Bit#(XLEN))) ;
    let shamt = imm & (fromInteger(valueOf(XLEN))-1);
    if ((shamt & 1) > 0)  src1 = src1 | ((src1 & fromInteger(mask_1_left_shift)) << 1)  | ((src1 & fromInteger(mask_1_right_shift)) >> 1);
    if ((shamt & 2) > 0)  src1 = src1 | ((src1 & fromInteger(mask_2_left_shift)) << 2)  | ((src1 & fromInteger(mask_2_right_shift)) >> 2);
    if ((shamt & 4) > 0)  src1 = src1 | ((src1 & fromInteger(mask_4_left_shift)) << 4)  | ((src1 & fromInteger(mask_4_right_shift)) >> 4);
    if ((shamt & 8) > 0)  src1 = src1 | ((src1 & fromInteger(mask_8_left_shift)) << 8)  | ((src1 & fromInteger(mask_8_right_shift)) >> 8);
    if ((shamt & 16) > 0) src1 = src1 | ((src1 & fromInteger(mask_16_left_shift)) << 16) | ((src1 & fromInteger(mask_16_right_shift)) >> 16);
`ifdef RV64
    if ((shamt & 32) > 0) src1 = src1 | (((src1 & fromInteger(mask_32_left_shift)) << 32) | ((src1 & fromInteger(mask_32_right_shift)) >> 32));
`endif
    return src1;
endfunction

  /*doc:func: The Generalized Reverse (GREV) operation iteratively checks each bit i in the 2nd argument from
i = 0 to log 2 (XLEN) − 1, and if the corresponding bit is set, swaps each adjacent pair of 2 i bits.*/
function Bit#(XLEN) fn_grev(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    let shamt = src2 & (fromInteger(valueOf(XLEN))-1);
    if ((shamt & 1) > 0)  src1 = ((src1 & fromInteger(mask_1_left_shift)) << 1)  | ((src1 & fromInteger(mask_1_right_shift)) >> 1);
    if ((shamt & 2) > 0)  src1 = ((src1 & fromInteger(mask_2_left_shift)) << 2)  | ((src1 & fromInteger(mask_2_right_shift)) >> 2);
    if ((shamt & 4 )> 0)  src1 = ((src1 & fromInteger(mask_4_left_shift)) << 4)  | ((src1 & fromInteger(mask_4_right_shift)) >> 4);
    if ((shamt & 8) > 0)  src1 = ((src1 & fromInteger(mask_8_left_shift)) << 8)  | ((src1 & fromInteger(mask_8_right_shift)) >> 8);
    if ((shamt & 16) > 0) src1 = ((src1 & fromInteger(mask_16_left_shift)) << 16) | ((src1 & fromInteger(mask_16_right_shift)) >> 16);
`ifdef RV64
    if ((shamt & 32) > 0) src1 = src1 | (((src1 & fromInteger(mask_32_left_shift)) << 32) | ((src1 & fromInteger(mask_32_right_shift)) >> 32));
`endif
    return src1;
endfunction

    /*doc:func: The Generalized Reverse (GREV) operation iteratively checks each bit i in the 2nd argument from
i = 0 to log 2 (XLEN) − 1, and if the corresponding bit is set, swaps each adjacent pair of 2 i bits.*/
function Bit#(XLEN) fn_grevi(Bit#(XLEN) src1,Imm imm) provisos(Bitwise#(Bit#(XLEN))) ;
    let shamt = imm & (fromInteger(valueOf(XLEN))-1);
    if ((shamt & 1) > 0)  src1 = ((src1 & fromInteger(mask_1_left_shift)) << 1)  | ((src1 & fromInteger(mask_1_right_shift)) >> 1);
    if ((shamt & 2) > 0)  src1 = ((src1 & fromInteger(mask_2_left_shift)) << 2)  | ((src1 & fromInteger(mask_2_right_shift)) >> 2);
    if ((shamt & 4) > 0)  src1 = ((src1 & fromInteger(mask_4_left_shift)) << 4)  | ((src1 & fromInteger(mask_4_right_shift)) >> 4);
    if ((shamt & 8) > 0)  src1 = ((src1 & fromInteger(mask_8_left_shift)) << 8)  | ((src1 & fromInteger(mask_8_right_shift)) >> 8);
    if ((shamt & 16) > 0) src1 = ((src1 & fromInteger(mask_16_left_shift)) << 16) | ((src1 & fromInteger(mask_16_right_shift)) >> 16);
`ifdef RV64
    if ((shamt & 32) > 0) src1 = src1 | (((src1 & fromInteger(mask_32_left_shift)) << 32) | ((src1 & fromInteger(mask_32_right_shift)) >> 32));
`endif
    return src1;
endfunction

/*doc:func:A generalized (un)shuffle operation has log 2 (XLEN) − 1 control bits, one for each pair of neighbouring bits in a bit index. When the bit is set, generalized shuffle will swap the two index bits.;*/
function Bit#(XLEN) shuffle_stage(Bit#(XLEN) src1, Bit#(XLEN) maskL, Bit#(XLEN) maskR, Integer n) provisos(Bitwise#(Bit#(XLEN))) ;
    let x = src1 & ~(maskL | maskR);
    x = x | ((src1 << n) & maskL) | ((src1 >> n) & maskR);
    return x;
endfunction

   /*doc:func:The shfl operation performs this swaps in MSB-to-LSB order (performing a rotate left shift on
contiguous regions of set control bits)*/
function Bit#(XLEN) fn_shfl(Bit#(XLEN) src1, Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    let x= src1;
    let shamt = src2 & `ifdef RV64 31 `else 15 `endif ;
`ifdef RV64
    if ((shamt & 16) > 0) x =shuffle_stage(x, fromInteger(mask_16_left_shfl), fromInteger(mask_16_right_shfl), 16);
`endif
    if ((shamt & 8 )> 0) x =shuffle_stage(x, fromInteger(mask_8_left_shfl), fromInteger(mask_8_right_shfl), 8);
    if ((shamt & 4 )> 0) x =shuffle_stage(x, fromInteger(mask_4_left_shfl), fromInteger(mask_4_right_shfl), 4);
    if ((shamt & 2) > 0) x =shuffle_stage(x, fromInteger(mask_2_left_shfl), fromInteger(mask_2_right_shfl), 2);
    if ((shamt & 1) > 0) x =shuffle_stage(x, fromInteger(mask_1_left_shfl), fromInteger(mask_1_right_shfl), 1);
    return x;
endfunction

    /*doc:func: the unshfl operation performs the swaps in LSB-to-MSB order (performing a rotate right shift on contiguous regions of set control bits).*/
function Bit#(XLEN) fn_unshfl(Bit#(XLEN) src1, Bit#(XLEN) src2) provisos(Bitwise#(Bit#(XLEN))) ;
    let x= src1;
    let shamt = src2 & `ifdef RV64 31 `else 15 `endif ;
    if ((shamt & 1 )> 0) x =shuffle_stage(x, fromInteger(mask_1_left_shfl), fromInteger(mask_1_right_shfl), 1);
    if ((shamt & 2 )> 0) x =shuffle_stage(x, fromInteger(mask_2_left_shfl), fromInteger(mask_2_right_shfl), 2);
    if ((shamt & 4) > 0) x =shuffle_stage(x, fromInteger(mask_4_left_shfl), fromInteger(mask_4_right_shfl), 4);
    if ((shamt & 8 )> 0) x =shuffle_stage(x, fromInteger(mask_8_left_shfl), fromInteger(mask_8_right_shfl), 8);
`ifdef RV64
    if ((shamt & 16) > 0) x =shuffle_stage(x, fromInteger(mask_16_left_shfl), fromInteger(mask_16_right_shfl), 16);
`endif
    return x;
endfunction

    /*doc:func:The shfl operation performs this swaps in MSB-to-LSB order (performing a rotate left shift on
contiguous regions of set control bits)*/
function Bit#(XLEN) fn_shfli(Bit#(XLEN) src1, Bit#(6) imm) provisos(Bitwise#(Bit#(XLEN))) ;
    let x= src1;
    let shamt = imm & `ifdef RV64 31 `else 15 `endif ;
`ifdef RV64
    if ((shamt & 16) > 0) x =shuffle_stage(x, fromInteger(mask_16_left_shfl), fromInteger(mask_16_right_shfl), 16);
`endif
    if ((shamt & 8 )> 0) x =shuffle_stage(x, fromInteger(mask_8_left_shfl), fromInteger(mask_8_right_shfl), 8);
    if ((shamt & 4 )> 0) x =shuffle_stage(x, fromInteger(mask_4_left_shfl), fromInteger(mask_4_right_shfl), 4);
    if ((shamt & 2) > 0) x =shuffle_stage(x, fromInteger(mask_2_left_shfl), fromInteger(mask_2_right_shfl), 2);
    if ((shamt & 1) > 0) x =shuffle_stage(x, fromInteger(mask_1_left_shfl), fromInteger(mask_1_right_shfl), 1);
    return x;
endfunction

    /*doc:func: the unshfl operation performs the swaps in LSB-to-MSB order (performing a rotate right shift on contiguous regions of set control bits).*/
function Bit#(XLEN) fn_unshfli(Bit#(XLEN) src1, Bit#(6) imm) provisos(Bitwise#(Bit#(XLEN))) ;
    let x= src1;
    let shamt = imm & `ifdef RV64 31 `else 15 `endif ;
    if ((shamt & 1 )> 0) x =shuffle_stage(x, fromInteger(mask_1_left_shfl), fromInteger(mask_1_right_shfl), 1);
    if ((shamt & 2 )> 0) x =shuffle_stage(x, fromInteger(mask_2_left_shfl), fromInteger(mask_2_right_shfl), 2);
    if ((shamt & 4) > 0) x =shuffle_stage(x, fromInteger(mask_4_left_shfl), fromInteger(mask_4_right_shfl), 4);
    if ((shamt & 8 )> 0) x =shuffle_stage(x, fromInteger(mask_8_left_shfl), fromInteger(mask_8_right_shfl), 8);
`ifdef RV64
    if ((shamt & 16) > 0) x =shuffle_stage(x, fromInteger(mask_16_left_shfl), fromInteger(mask_16_right_shfl), 16);
`endif
    return x;
endfunction

`ifdef RV64
   /*doc:func:The GORC operation is similar to GREV, except that instead of swapping pairs of bits, GORC
ORs them together, and writes the new value in both positions.; for 64 bits*/
function Bit#(XLEN) fn_gorcw(Bit#(XLEN_BY_2) src1,Bit#(XLEN_BY_2) src2) provisos(Bitwise#(Bit#(XLEN_BY_2)),Add#(a__, XLEN_BY_2, 64)) ;
    let shamt = src2 & (fromInteger(valueOf(XLEN_BY_2))-1);
    if ((shamt & 1) > 0)  src1 = src1 | ((src1 & fromInteger(w_mask_1_left_shift)) << 1)  | ((src1 & fromInteger(w_mask_1_right_shift)) >> 1);
    if ((shamt & 2) > 0)  src1 = src1 | ((src1 & fromInteger(w_mask_2_left_shift)) << 2)  | ((src1 & fromInteger(w_mask_2_right_shift)) >> 2);
    if ((shamt & 4 )> 0)  src1 = src1 | ((src1 & fromInteger(w_mask_4_left_shift)) << 4)  | ((src1 & fromInteger(w_mask_4_right_shift)) >> 4);
    if ((shamt & 8) > 0)  src1 = src1 | ((src1 & fromInteger(w_mask_8_left_shift)) << 8)  | ((src1 & fromInteger(w_mask_8_right_shift)) >> 8);
    if ((shamt & 16) > 0) src1 = src1 | ((src1 & fromInteger(w_mask_16_left_shift)) << 16) | ((src1 & fromInteger(w_mask_16_right_shift)) >> 16);
    return extend(src1);
endfunction

  /*doc:func:The GORC operation is similar to GREV, except that instead of swapping pairs of bits, GORC
ORs them together, and writes the new value in both positions.; for 64 bits*/
function Bit#(XLEN) fn_gorciw(Bit#(XLEN_BY_2) src1,Imm1 imm) provisos(Bitwise#(Bit#(XLEN_BY_2)),Add#(a__, XLEN_BY_2, 64)) ;
    let shamt = {3'b000,imm} & (fromInteger(valueOf(XLEN))-1);
    if ((shamt & 1) > 0)  src1 = src1 | ((src1 & fromInteger(w_mask_1_left_shift)) << 1)  | ((src1 & fromInteger(w_mask_1_right_shift)) >> 1);
    if ((shamt & 2) > 0)  src1 = src1 | ((src1 & fromInteger(w_mask_2_left_shift)) << 2)  | ((src1 & fromInteger(w_mask_2_right_shift)) >> 2);
    if ((shamt & 4 )> 0)  src1 = src1 | ((src1 & fromInteger(w_mask_4_left_shift)) << 4)  | ((src1 & fromInteger(w_mask_4_right_shift)) >> 4);
    if ((shamt & 8) > 0)  src1 = src1 | ((src1 & fromInteger(w_mask_8_left_shift)) << 8)  | ((src1 & fromInteger(w_mask_8_right_shift)) >> 8);
    if ((shamt & 16) > 0) src1 = src1 | ((src1 & fromInteger(w_mask_16_left_shift)) << 16) | ((src1 & fromInteger(w_mask_16_right_shift)) >> 16);
    return extend(src1);
endfunction

  /*doc:func: The Generalized Reverse (GREV) operation iteratively checks each bit i in the 2nd argument from
i = 0 to log 2 (XLEN) − 1, and if the corresponding bit is set, swaps each adjacent pair of 2 i bits.; for 64 bits*/
function Bit#(XLEN) fn_grevw(Bit#(XLEN_BY_2) src1,Bit#(XLEN_BY_2) src2) provisos(Bitwise#(Bit#(XLEN_BY_2)),Add#(a__, XLEN_BY_2, 64)) ;
    let shamt = src2 & (fromInteger(valueOf(XLEN))-1);
    if ((shamt & 1) > 0)  src1 = ((src1 & fromInteger(w_mask_1_left_shift)) << 1)  | ((src1 & fromInteger(w_mask_1_right_shift)) >> 1);
    if ((shamt & 2) > 0)  src1 = ((src1 & fromInteger(w_mask_2_left_shift)) << 2)  | ((src1 & fromInteger(w_mask_2_right_shift)) >> 2);
    if ((shamt & 4) > 0)  src1 = ((src1 & fromInteger(w_mask_4_left_shift)) << 4)  | ((src1 & fromInteger(w_mask_4_right_shift)) >> 4);
    if ((shamt & 8) > 0)  src1 = ((src1 & fromInteger(w_mask_8_left_shift)) << 8)  | ((src1 & fromInteger(w_mask_8_right_shift)) >> 8);
    if ((shamt & 16) > 0) src1 = ((src1 & fromInteger(w_mask_16_left_shift)) << 16) | ((src1 & fromInteger(w_mask_16_right_shift)) >> 16);
    return extend(src1);
endfunction

   /*doc:func: The Generalized Reverse (GREV) operation iteratively checks each bit i in the 2nd argument from
i = 0 to log 2 (XLEN) − 1, and if the corresponding bit is set, swaps each adjacent pair of 2 i bits.; for 64 bits*/
function Bit#(XLEN) fn_greviw(Bit#(XLEN_BY_2) src1,Imm1 imm) provisos(Bitwise#(Bit#(XLEN_BY_2)),Add#(a__, XLEN_BY_2, 64)) ;
    let shamt = {3'b000,imm} & (fromInteger(valueOf(XLEN))-1);
    if ((shamt & 1) > 0)  src1 =  ((src1 & fromInteger(w_mask_1_left_shift)) << 1) | ((src1 & fromInteger(w_mask_1_right_shift)) >> 1);
    if ((shamt & 2) > 0)  src1 =  ((src1 & fromInteger(w_mask_2_left_shift)) << 2) | ((src1 & fromInteger(w_mask_2_right_shift)) >> 2);
    if ((shamt & 4) > 0)  src1 =  ((src1 & fromInteger(w_mask_4_left_shift)) << 4) | ((src1 & fromInteger(w_mask_4_right_shift)) >> 4);
    if ((shamt & 8) > 0)  src1 =  ((src1 & fromInteger(w_mask_8_left_shift)) << 8) | ((src1 & fromInteger(w_mask_8_right_shift)) >> 8);
    if ((shamt & 16) > 0) src1 =  ((src1 & fromInteger(w_mask_16_left_shift)) << 16)| ((src1 & fromInteger(w_mask_16_right_shift)) >> 16);
    return signExtend(src1);
endfunction

function Bit#(XLEN_BY_2) shuffle_stagew(Bit#(XLEN_BY_2) src1, Bit#(XLEN_BY_2) maskL, Bit#(XLEN_BY_2) maskR, Integer n) provisos(Bitwise#(Bit#(XLEN_BY_2))) ;
    let x = src1 & ~(maskL | maskR);
    x = x | ((src1 << n) & maskL) | ((src1 >> n) & maskR);
    return x;
endfunction

   /*doc:func:The shfl operation performs this swaps in MSB-to-LSB order (performing a rotate left shift on
contiguous regions of set control bits)*/
function Bit#(XLEN) fn_shflw(Bit#(XLEN_BY_2) src1, Bit#(XLEN_BY_2) src2) provisos(Bitwise#(Bit#(XLEN_BY_2)),Add#(a__, XLEN_BY_2, 64)) ;
    let x= src1;
    let shamt = src2 & 15 ;
    if ((shamt & 8 )> 0) x = shuffle_stagew(x, fromInteger(w_mask_8_left_shfl), fromInteger(w_mask_8_right_shfl), 8);
    if ((shamt & 4 )> 0) x = shuffle_stagew(x, fromInteger(w_mask_4_left_shfl), fromInteger(w_mask_4_right_shfl), 4);
    if ((shamt & 2) > 0) x = shuffle_stagew(x, fromInteger(w_mask_2_left_shfl), fromInteger(w_mask_2_right_shfl), 2);
    if ((shamt & 1) > 0) x = shuffle_stagew(x, fromInteger(w_mask_1_left_shfl), fromInteger(w_mask_1_right_shfl), 1);
    return signExtend(x);
endfunction

    /*doc:func: the unshfl operation performs the swaps in LSB-to-MSB order (performing a rotate right shift on contiguous regions of set control bits).*/
function Bit#(XLEN) fn_unshflw(Bit#(XLEN_BY_2) src1, Bit#(XLEN_BY_2) src2) provisos(Bitwise#(Bit#(XLEN_BY_2)),Add#(a__, XLEN_BY_2, 64)) ;
    let x= src1;
    let shamt = src2 & 15 ;
    if ((shamt & 1 )> 0) x = shuffle_stagew(x, fromInteger(w_mask_1_left_shfl), fromInteger(w_mask_1_right_shfl), 1);
    if ((shamt & 2 )> 0) x = shuffle_stagew(x, fromInteger(w_mask_2_left_shfl), fromInteger(w_mask_2_right_shfl), 2);
    if ((shamt & 4) > 0) x = shuffle_stagew(x, fromInteger(w_mask_4_left_shfl), fromInteger(w_mask_4_right_shfl), 4);
    if ((shamt & 8 )> 0) x = shuffle_stagew(x, fromInteger(w_mask_8_left_shfl), fromInteger(w_mask_8_right_shfl), 8);
    return signExtend(x);
endfunction
   `endif


