/*doc:func: function that extract / clear / set / invert only the bit in rs1 whose position can
be determined with  the help of the value in rs2  */
function Bit#(XLEN) fn_set_bit_ops( Bit#(XLEN) src1,Bit#(TLog#(XLEN)) bitpos,Bit#(1) w_inst,Bit#(3) oper) provisos(Bitwise#(Bit#(XLEN))) ;
`ifdef RV64
    if(w_inst==1)begin
        bitpos = bitpos & 31 ;
        src1=zeroExtend(src1[31:0]);
    end
`endif
    case(oper) matches
        3'b??1:   src1 = extend(src1[bitpos]);        //SBEXT
        3'b0??:   src1[bitpos]=1;             //SBSET
        3'b?0?:   src1[bitpos]=0;             //SBCLR
        3'b11?:   src1[bitpos]=~src1[bitpos]; //SBINV
    endcase
    return (unpack(w_inst)) ? signExtend(src1[31:0]) : (src1);
endfunction
