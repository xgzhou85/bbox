 /*doc:func: shift rs1 left by 1 bit, then add the result to rs2*/
 function Bit#(XLEN) fn_sh1add(Bit#(XLEN) src1,Bit#(XLEN) src2,Bit#(1) op3) provisos(Arith#(Bit#(XLEN))) ; 
`ifdef RV64    if(op3==1) src1 = src1 & 64'h00000000ffffffff ;`endif
    return ((src1 << 1) + src2 );
  endfunction
  /*doc:func: shift rs1 left by 2 bit, then add the result to rs2*/  
  function Bit#(XLEN) fn_sh2add(Bit#(XLEN) src1,Bit#(XLEN) src2,Bit#(1) op3) provisos(Arith#(Bit#(XLEN))) ; 
   `ifdef RV64    if(op3==1) src1 = src1 & 64'h00000000ffffffff ;`endif
    return ((src1 << 2) + src2 );
  endfunction
  /*doc:func: shift rs1 left by 3 bit, then add the result to rs2*/  
  function Bit#(XLEN) fn_sh3add(Bit#(XLEN) src1,Bit#(XLEN) src2,Bit#(1) op3) provisos(Arith#(Bit#(XLEN))) ; 
  `ifdef RV64    if(op3==1) src1 = src1 & 64'h00000000ffffffff ;`endif
    return ((src1 << 3) + src2 );
  endfunction

  `ifdef RV64
  /*doc:func: The sh?addu.w instructions are identical to sh?add, except that bits XLEN-1:32 of the rs1 argument are cleared
before the shift.*/
  function Bit#(XLEN) fn_sh1adduw(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Arith#(Bit#(XLEN))) ; 
    src1 = src1 & 64'h00000000ffffffff ;
    return zeroExtend((src1 << 1) + src2 );
  endfunction
  /*doc:func: The sh?addu.w instructions are identical to sh?add, except that bits XLEN-1:32 of the rs1 argument are cleared
before the shift.*/
  function Bit#(XLEN) fn_sh2adduw(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Arith#(Bit#(XLEN))) ; 
    src1 = src1 & 64'h00000000ffffffff ;
    return zeroExtend((src1 << 2) + src2 );
  endfunction
  /*doc:func: The sh?addu.w instructions are identical to sh?add, except that bits XLEN-1:32 of the rs1 argument are cleared
before the shift.*/
  function Bit#(XLEN) fn_sh3adduw(Bit#(XLEN) src1,Bit#(XLEN) src2) provisos(Arith#(Bit#(XLEN))) ; 
    src1 = src1 & 64'h00000000ffffffff ;
    return zeroExtend((src1 << 3) + src2 );
  endfunction
  `endif
